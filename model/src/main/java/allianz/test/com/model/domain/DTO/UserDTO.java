package allianz.test.com.model.domain.DTO;

import java.io.Serializable;
import java.util.Objects;

public class UserDTO implements Serializable {

    private Integer id;
    private String label;
    private String date;

    public UserDTO() {
    }

    public UserDTO(Integer id, String label, String date) {
        this.id = id;
        this.label = label;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        if (label != null && !label.isEmpty()) {
            this.label = label;
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        if (date != null && !date.isEmpty()) {
            this.date = date;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(id, userDTO.id) &&
                Objects.equals(label, userDTO.label) &&
                Objects.equals(date, userDTO.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label, date);
    }
}

