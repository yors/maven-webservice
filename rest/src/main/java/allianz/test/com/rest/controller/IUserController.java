package allianz.test.com.rest.controller;

import allianz.test.com.model.domain.DTO.UserDTO;

import java.util.List;

/**
 *  Controller contract
 */
public interface IUserController {

    /**
     * @param user the user to add
     * @return {@link UserDTO}
     */
    UserDTO createUser(UserDTO user);


    /**
     * Get All user
     *
     * @return list of {@link UserDTO}
     */
    List<UserDTO> getAllUsers();


    /**
     * @param id user identifier
     * @return {@link UserDTO}
     */
    UserDTO getUser(final int id);


    /**
     * @param id     user identifier
     * @param member the user
     * @return {@link UserDTO}
     */
    UserDTO updateUser(final int id, UserDTO member);


    /**
     * @param id user identifier
     */
    void deleteUser(final int id);

}
