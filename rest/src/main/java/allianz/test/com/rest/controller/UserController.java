package allianz.test.com.rest.controller;


import allianz.test.com.model.domain.DTO.UserDTO;
import allianz.test.com.rest.session.SessionData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/mavenTest")
public class UserController implements IUserController  {


    private final SessionData sessionData;

    @Autowired
    public UserController(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    @PostMapping("/users")
    public UserDTO createUser(@RequestBody UserDTO user) {
        sessionData.addUser(user);
        return sessionData.getTheLastUserAdded();
    }

    @GetMapping("users")
    public List<UserDTO> getAllUsers(){
        return  sessionData.getUsers();
    }


    @GetMapping("users/{id}")
    public UserDTO getUser(@PathVariable("id") final int id){
        return sessionData.getUserById(id);

    }



    @PutMapping("users/{id}")
    public UserDTO updateUser(@PathVariable("id") final int id, @RequestBody UserDTO member) {

        sessionData.updateUserById(member);
        return sessionData.getUserById(id);
    }


    @DeleteMapping("users/{id}")
    public void deleteUser(@PathVariable("id") final int id) {
        sessionData.deleteUserById(id);
    }


}