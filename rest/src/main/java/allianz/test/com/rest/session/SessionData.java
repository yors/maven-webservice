package allianz.test.com.rest.session;

import allianz.test.com.model.domain.DTO.UserDTO;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class SessionData implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<UserDTO> users = new ArrayList<>();

    /**
     * Add a user
     *
     * @param user the user to add
     */
    public void addUser(UserDTO user){
        this.users.add(user);
    }

    /**
     * Get the last user added
     *
     * @return {@link UserDTO}
     */
    public UserDTO getTheLastUserAdded(){
        if(users.isEmpty()){
            return new UserDTO(null,null,null);
        }
        return this.users.get(this.users.size()-1);
    }


    /**
     *  Get all users
     * @return {@link UserDTO}
     */
    public List<UserDTO> getUsers(){
        return users;
    }

    /**
     *  Delete a user
     *
     * @param id user identifier
     */
    public void deleteUserById(int id){
        this.users.removeIf(e -> e.getId() == id);
    }


    /**
     * Update user data
     *
     * @param user user to update
     *
     */
    public void updateUserById(UserDTO user){
        if (user != null) {

            this.users= users.stream().map(x->{
                if(x.getId() == user.getId())
                {
                    x.setDate(user.getDate());
                    x.setLabel(user.getLabel());
                }
                return x;
            }).collect(Collectors.toList());
        }
    }


    /**
     * Get user by the identifier
     *
     * @param id user identifier
     *
     * return {@link UserDTO}
     */
    public UserDTO getUserById(int id){
        Optional<UserDTO> user = users.stream().filter(e -> e.getId() == id).findFirst();
        if(user.isPresent())
        {
            return user.get();
        }
        return new UserDTO(null,null,null);
    }


}
