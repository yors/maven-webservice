package allianz.test.com.rest.controller;

import allianz.test.com.model.domain.DTO.UserDTO;
import allianz.test.com.rest.RestApplication;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * Test class for Rest controller Api
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = RestApplication.class)
public class RestControllerIntegrationApiTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private static final String RESOURCE_PATH = "/mavenTest";

    private String REQUEST_URI;

    private HttpEntity<?> entity;

    private HttpHeaders headers;

    @BeforeEach
    public void initEntityHttp() {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        entity = new HttpEntity<>(headers);
    }


    @Test
    public void testCreateUserShouldReturnDefaultValue() {

        String operationPath = "/users";
        this.REQUEST_URI = "http://localhost:" + port + RESOURCE_PATH;
        UserDTO user = new UserDTO(1,"label1","2021-10-02");
        entity = new HttpEntity<>(user,headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(this.REQUEST_URI + operationPath, HttpMethod.POST, entity, UserDTO.class);
        Assertions.assertThat(response.getBody()).isEqualTo(user);
    }

    @Test
    public void testGetUserByIdShouldReturnDefaultValue() {

        String operationPath = "/users/1";
        this.REQUEST_URI = "http://localhost:" + port + RESOURCE_PATH;
        ResponseEntity<UserDTO> response = restTemplate.exchange(this.REQUEST_URI + operationPath, HttpMethod.GET, entity, UserDTO.class);
        assertNotNull("Must not be  null", response.getBody());
    }


    @Test
    public void testGetAllUserByIdShouldReturnDefaultValue() {

        String operationPath = "/users";
        this.REQUEST_URI = "http://localhost:" + port + RESOURCE_PATH;
        ResponseEntity<List> response = restTemplate.exchange(this.REQUEST_URI + operationPath, HttpMethod.GET, entity,List.class);
        assertNotNull("Must be not null", response.getBody());
    }

    @Test
    public void testUpdateUserShouldReturnDefaultValue() {

        String operationPath = "/users/1";
        this.REQUEST_URI = "http://localhost:" + port + RESOURCE_PATH;
        entity = new HttpEntity<>(new UserDTO(1,"label","toto"), headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(this.REQUEST_URI + operationPath, HttpMethod.PUT, entity, UserDTO.class);
        assertNotNull("Must be not  null", response.getBody());
    }




}
