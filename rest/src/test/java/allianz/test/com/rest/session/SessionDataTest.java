package allianz.test.com.rest.session;

import allianz.test.com.model.domain.DTO.UserDTO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class SessionDataTest {

    SessionData sessionData;
    @BeforeEach
    public void initSession() {
        sessionData = new SessionData();
    }

    @Test
    public void addUserTest(){
        sessionData.addUser(new UserDTO(1,"",""));
        Assertions.assertThat(sessionData.getUsers()).isNotEmpty();
    }

    @Test
    public void getTheLastUserAddedTest(){
        UserDTO user = new UserDTO(1,"test","test");
        sessionData.addUser(new UserDTO(1,"",""));
        sessionData.addUser(user);
        Assertions.assertThat(sessionData.getTheLastUserAdded()).isEqualTo(user);
    }


    @Test
    public void getUserById(){
        UserDTO user = new UserDTO(2,"test","test");
        sessionData.addUser(user);
        sessionData.addUser(new UserDTO(3,"",""));
        Assertions.assertThat( sessionData.getUserById(2)).isEqualTo(user);
    }

    @Test
    public void deleteByIdTest(){
        UserDTO user = new UserDTO(2,"test","test");
        sessionData.addUser(user);
        sessionData.addUser(new UserDTO(3,"",""));
        sessionData.deleteUserById(3);
        Assertions.assertThat( sessionData.getUsers()).hasSize(1);
    }


    @Test
    public void updateUserById(){
        UserDTO user = new UserDTO(2,"test","test");
        sessionData.addUser(user);
        user.setLabel("label");
        user.setDate("date");
        sessionData.updateUserById(user);
        Assertions.assertThat( sessionData.getUserById(2)).isEqualTo(user);
    }



}
